echo "Environmental construction..."

echo -n "Setting?(git pull, overwrite conf files) [y...1/n...0]: "
read ans

if test ${ans} -eq 1; then

    rm ~/.emacs.d/init.el
    rm ~/.zshrc
    rm ~/.tmux.conf
    rm ~/.vimrc
    
    emacsdir=~/.emacs.d
    
    echo "Cloning files from git repository..."
    git pull

    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git
    
    if [ -e ${emacsdir} ]; then
        echo "${emacsdir}, already exist!"
    else
        echo "${emacsdir}, NOT found."
        echo "make directory ${emacsdir}"
        mkdir ${emacsdir}
    fi
    
    ln -s ~/.dotfiles/emacs ~/.emacs.d/init.el
    ln -s ~/.dotfiles/zsh ~/.zshrc
    zcompile ~/.zshrc
    ln -s ~/.dotfiles/tmux ~/.tmux.conf
    ln -s ~/.dotfiles/vim ~/.vimrc
    
    echo "done."
else
    echo "Nothing to do. Exit."
fi

