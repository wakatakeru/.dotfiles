################################################
##
## zsh config
## Takeru SHIMIZU
## Sep 14, 2015
## Update: Jun 18, 2017
##
################################################

# プロンプト表示設定
setopt prompt_subst

# 色をつける
autoload colors
colors

# 右側にホスト名を表示
RPROMPT="%B%(?.%{$fg_bold[blue]%}.%{$fg_bold[white]$bg[red]%})[${HOST}]%{$reset_color%}"

# 誤入力補完
setopt correct
SPROMPT="%{$fg[magenta]%}Command not found. Perhaps, %B%{${fg[cyan]}%}%r%b%{$fg[magenta]%} ? [Yes(y),No(n),a,e]:%{${reset_color}"

# カレントディレクトリの表示
PROMPT="
%{$fg_bold[cyan]%}%d$reset_color%}
%(?.%{$fg_bold[green]%}.%{${fg_bold[red]}%})%n > "

PROMPT2='[%n]> '

# Gitブランチの表示
autoload -Uz vcs_info
setopt prompt_subst
zstyle ':vcs_info:git:*' check-for-changes true
zstyle ':vcs_info:git:*' stagedstr "%F{yellow}!"
zstyle ':vcs_info:git:*' unstagedstr "%F{red}+"
zstyle ':vcs_info:*' formats "%F{green}%c%u[%b]%f"
zstyle ':vcs_info:*' actionformats '[%b|%a]'
precmd () { vcs_info }
RPROMPT='${vcs_info_msg_0_}'" "$RPROMPT

# 自動補完
autoload -U compinit; compinit

# lsに色を付ける
export LSCOLORS=gxfxxxxxcxxxxxxxxxgxgx
export LS_COLORS='di=01;36:ln=01;35:ex=01;32'
zstyle ':completion:*' list-colors 'di=36' 'ln=35' 'ex=32'

case "${OSTYPE}" in
    freebsd*|darwin*)
        alias ls="ls -GF"
        ;;
    linux*)
        alias ls="ls -F --color"
        ;;
esac

# alias conf

alias la='ls -a'
alias ll='ls -l'
alias lla='ls -al'
alias kusonemi='tw --yes クソネミ'
alias mailer='alpine'
alias gs='git status'
alias emacs='emacs -nw'
alias sl='sl; tw --yes "slコマンドが実行されました"'
alias gch='git checkout'
alias be='bundle exec'
alias ga='git add'
alias gc='git commit'

# 補完候補をハイライト
zstyle ':completion:*:default' menu select=0
zstyle ':completion:*' verbose yes
zstyle ':completion:*' completer _expand _complete _match _prefix _approximate _list _history
zstyle ':completion:*:messages' format $YELLOW'%d'$DEFAULT
zstyle ':completion:*:warnings' format $RED'No matches for:'$YELLOW' %d'$DEFAULT
zstyle ':completion:*:descriptions' format $YELLOW'completing %B%d%b'$DEFAULT
zstyle ':completion:*:corrections' format $YELLOW'%B%d '$RED'(errors: %e)%b'$DEFAULT
zstyle ':completion:*:options' description 'yes'

# 候補に色を付ける
export LS_COLORS='di=34:ln=35:so=32:pi=33:ex=31:bd=46;34:cd=43;34:su=41;30:sg=46;30:tw=42;30:ow=43;30'
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

# コマンドのシンタックスハイライト
source ~/.dotfiles/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
if [ -z $TMUX ]; then
export PATH="/usr/local/sbin:$PATH"
fi

# ビープ消音
setopt nolistbeep

# ローカル設定の読み込み
[ -f ~/.zshrc.local ] && source ~/.zshrc.local