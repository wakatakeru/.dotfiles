# SetUp  
1. This directory arrange "~/".  
2. Run "./setting.sh"

# Contents
1. Emacs config
2. Zsh config
3. tmux config
4. Vim config

# Supplemental
If you need local setting of zsh, you can put .zshrc.local on ~ directory.