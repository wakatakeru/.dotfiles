;;--------------------------------------
;; Emacs Configuration
;; 2015.12.11
;; Update: Oct 30, 2018
;; Takeru SHIMIZU
;;--------------------------------------

;; スタート時のスプラッシュ非表示
(setq inhibit-startup-message t)

;; パッケージの設定
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/") t)
(package-initialize)
(unless package-archive-contents (package-refresh-contents))
(defvar installing-package-list
  '(
    auto-complete
    go-mode
    nlinum-mode
    php-mode
    fuzzy
    yasnippet
    ))


;; 文字コードを日本語にする
(set-language-environment "japanese")
(prefer-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-buffer-file-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)

;; 対応する括弧を強調
(show-paren-mode t)
(setq show-paren-delay 0)                              ; 表示までの秒数: 0秒
(setq show-paren-style 'expression)                    ; カッコ内強調の有効化

;; バックアップファイルを作成しない
(setq make-backup-files nil)
(setq auto-save-default nil)
(setq backup-inhibited t)

;; 行番号表示
(global-nlinum-mode t)
(set-face-attribute 'linum nil
                    :foreground "#33FF00"
                    :background "#404040"
                    :height 0.5)
(setq nlinum-format "%3d ")

;; 色を付ける
(global-font-lock-mode t)
(add-to-list 'default-frame-alist '(foreground-color . "#ababab"))

;; 補完時に大文字小文字を区別しない
(setq completion-ignore-case t)

;; スクロールを3行ずつにする
(setq scroll-step 3)

;; 画面右端で折り返す
(setq-default truncate-lines nil)
(setq truncate-partial-width-windows nil)

;; バッファの最後でnewlineで新規行を追加するのを禁止する
(setq next-line-add-newlines nil)

;; 現在の関数名をモードラインに表示
(which-function-mode 1)

;; タブをスペース2字
(setq default-tab-width 2)
(setq-default indent-tabs-mode nil)

;; モードラインに日付・時刻を表示する
(setq display-time-string-forms
      '((format "%s/%s/%s(%s) %s:%s" year month day dayname 24-hours minutes)
        load
        (if mail " Mail" "")))

;; 時刻表示の左隣に日付を追加
(setq display-time-kawakami-form t)

;; 時刻の表示(24時間制)
(display-time)
(setq display-time-24hr-format t)

;;カッコを自動挿入
(electric-pair-mode 1)

;; auto-complete
(require 'auto-complete)
(require 'auto-complete-config)
(global-auto-complete-mode t)
(setq ac-auto-start t)
(ac-config-default)
(add-to-list 'ac-modes 'text-mode)         ;; text-modeでも自動的に有効にする
(add-to-list 'ac-modes 'fundamental-mode)  ;; fundamental-mode
(add-to-list 'ac-modes 'org-mode)
(add-to-list 'ac-modes 'yatex-mode)
(ac-set-trigger-key "TAB")
(setq ac-use-menu-map t)       ;; 補完メニュー表示時にC-n/C-pで補完候補選択
(setq ac-use-fuzzy t)          ;; 曖昧マッチ

;; ジャンプ機能
(global-set-key "\C-xj" 'goto-line)

;; カレント行のハイライト
(require 'hl-line)
(defun global-hl-line-timer-function ()
  (global-hl-line-unhighlight-all)
  (let ((global-hl-line-mode t))
    (global-hl-line-highlight)))
(setq global-hl-line-timer
      (run-with-idle-timer 0.01 t 'global-hl-line-timer-function))
(custom-set-faces
 '(font-lock-function-name-face ((t (:foreground "blue"))))
 '(hl-line ((t (:background "#202020")))))

;; yasnippet
(add-to-list 'load-path "~/.emacs.d/yasnippet")
(require 'yasnippet)
(yas-global-mode 1)

;; fuzzy
(require 'fuzzy)
(put 'downcase-region 'disabled nil)

;; rubyのマジックコメントを表示しない
(setq ruby-insert-encoding-magic-comment nil)

;; key-bind
(global-set-key (kbd "<down>")  'windmove-down)
(global-set-key (kbd "<up>")    'windmove-up)

;; hs-minor-mode
(add-hook 'c++-mode-hook
          '(lambda ()
             (hs-minor-mode 1)))
(add-hook 'c-mode-hook
          '(lambda ()
             (hs-minor-mode 1)))
(add-hook 'scheme-mode-hook
          '(lambda ()
             (hs-minor-mode 1)))
(add-hook 'emacs-lisp-mode-hook
          '(lambda ()
             (hs-minor-mode 1)))
(add-hook 'lisp-mode-hook
          '(lambda ()
             (hs-minor-mode 1)))
(add-hook 'python-mode-hook
          '(lambda ()
             (hs-minor-mode 1)))
(add-hook 'ruby-mode-hook
          '(lambda ()
             (hs-minor-mode 1)))
(add-hook 'xml-mode-hook
          '(lambda ()
             (hs-minor-mode 1)))

(define-key global-map (kbd "C-l") 'hs-toggle-hiding)
(custom-set-variables
 '(package-selected-packages
   (quote
    (go go-mode plantuml-mode yaml-mode markdown-mode+ markdown-mode js2-mode nlinum undo-tree rainbow-delimiters powerline hlinum highlight-unique-symbol highlight-symbol goto-chg fuzzy flycheck auto-highlight-symbol auto-complete))))

;; PHP-Mode
(require 'php-mode)

(add-hook 'go-mode-hook
          '(lambda ()
             (setq tab-width 2)
             ))
